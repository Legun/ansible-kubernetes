ansible Playbooks for install Kubernetes
========================================

source [https://www.8host.com/blog/sozdanie-klastera-kubernetes-1-10-s-pomoshhyu-kubeadm-v-ubuntu-16-04/]

**Sample Install APP

kubectl run nginx --image=nginx --port 80
kubectl expose deploy nginx --port 80 --target-port 80 --type NodePort
kubectl get services

http://worker_1_ip:nginx_port or http://worker_2_ip:nginx_port

**Sample Remove APP

kubectl delete service nginx
kubectl get services
kubectl delete deployment nginx
kubectl get deployments
